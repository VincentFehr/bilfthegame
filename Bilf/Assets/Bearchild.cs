﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bearchild : MonoBehaviour
{
    public GameObject carrytarget;
    private Rigidbody rb;
    private bool pickedup;
    public Vector3 adjustmentsv3;

    private float _timer = 0.2f;
   

    bool _pickUpBear;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        pickedup = false;
    }
    private void Update()
    {
        if(pickedup)
        {
                this.transform.position = carrytarget.transform.position;
                this.transform.localPosition = adjustmentsv3;
                this.transform.rotation = carrytarget.transform.rotation;                       
                this.transform.parent = carrytarget.transform;
                rb.isKinematic = true;
            
        }
      
        if(_pickUpBear)
        {
            _timer -= Time.deltaTime;
        }
        if (!pickedup)
        {
            rb.isKinematic = false;
            carrytarget.transform.DetachChildren();
            this.GetComponent<Collider>().enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.E) && _timer <= 0f)
        {
            if (pickedup == true)
            { 
                pickedup = false;
                _pickUpBear = false;
                _timer = 0.2f;

            }

        }

    }
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (pickedup == false)
            {
                if (other.gameObject.tag == "Mouth")
                {
                    Debug.Log("Bär wird aufgehoben");                   
                    pickedup = true;
                    _pickUpBear = true;
                    this.GetComponent<Collider>().enabled = false;

                }

            }
     
        }

    }

    //    private GameObject targetbear;

    //    private Rigidbody rb;
    //    [SerializeField]
    //    private GameObject carrytarget;
    //    private bool _pickUp;
    //    private bool _dropIt;
    //    private bool _droppedbear;
    //    [SerializeField]
    //    private Collider col;

    //    private Collider BearCol;
    //    [SerializeField]
    //    private float _dropTimer = 0.2f;
    //    [SerializeField]
    //    private float _grabTimer = 0.2f;


    //    void Start()
    //    {
    //        BearCol = this.targetbear.GetComponent<Collider>();
    //        rb = this.targetbear.GetComponent<Rigidbody>();
    //        targetbear = this.gameObject;
    //    }

    //    void Update()
    //    {
    //        HoldTree();
    //        DropTree();
    //        PickMeUpAgain();
    //    }
    //    private void OnTriggerStay(Collider Bearcol)
    //    {
    //        if (col.gameObject.tag == "Mouth")
    //        {
    //            if (this.tag == "Bearchild" && Input.GetKey(KeyCode.F))
    //            {
    //                _pickUp = true;
    //            }

    //        }
    //    }

    //    private void PickMeUpAgain()
    //    {
    //        if (_droppedbear == true)
    //        {
    //            _grabTimer -= Time.deltaTime;

    //            if (_grabTimer <= 0f)
    //            {
    //                _dropIt = false;
    //                _droppedbear = false;
    //            }
    //        }
    //    }
    //    private void DropTree()
    //    {
    //        if (_dropIt == true && Input.GetKey(KeyCode.F))
    //        {
    //            BearCol.enabled = true;
    //            rb.useGravity = true;
    //            _pickUp = false;
    //            targetbear.transform.parent = null;
    //            _dropTimer = 0.2f;
    //            _droppedbear = true;
    //        }
    //    }

    //    private void HoldTree()
    //    {
    //        if (_pickUp == true)
    //        {
    //            BearCol.enabled = false;
    //            targetbear.transform.position = carrytarget.transform.position;
    //            targetbear.transform.rotation = carrytarget.transform.rotation;
    //            targetbear.transform.parent = carrytarget.transform;
    //            rb.useGravity = false;
    //            _dropTimer -= Time.deltaTime;
    //            _grabTimer = 0.2f;

    //            if (_dropTimer <= 0f)
    //            {
    //                _dropIt = true;
    //            }
    //        }
    //    }
}
