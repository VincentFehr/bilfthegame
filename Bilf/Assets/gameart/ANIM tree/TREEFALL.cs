﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TREEFALL : MonoBehaviour
{
    private Animator Anim;
    public GameObject uppertree;
        public GameObject undertree;
    public GameObject Frontleaves;
    public float timetowait;
    public float killtime;
    

    private void Start()
    {

        Anim = this.GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Baum wurde getroffen");

        if (Input.GetMouseButton(0))
        {
            if (other.gameObject.tag == "Frontslashcollider_Player")
            {
                Debug.Log("Baum wurde getroffen");
                this.Anim.Play("fall");
                uppertree.GetComponent<Collider>().isTrigger = false;
                undertree.GetComponent<Collider>().isTrigger = false;
                
                Frontleaves.GetComponent<Rigidbody>().isKinematic = false;
                StartCoroutine(Waitforanim());
                
            }
        }
        
    }
    private IEnumerator Waitforanim()
    {
        yield return new WaitForSeconds(timetowait);
        StartCoroutine(killme());

    }
    private IEnumerator killme()
    {
        
        yield return new WaitForSeconds(killtime);
        Destroy(Frontleaves);
    }

    //PlayMonsterAnim.GetComponent<Animator>().GetCurrentAnimatorStateInfo().TagHash;

}
