﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorOn : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Sets Cursor to  be visible
        Cursor.visible = true;
        // unlocks Cursor
        Cursor.lockState = CursorLockMode.None;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
