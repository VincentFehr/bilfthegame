﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeToScene : MonoBehaviour
{
    // class to Escape Scenes with [Esc]

    // insert Scene you want to Escape to
    [SerializeField]
    public string sceneName;

    // loads Scene (sceneName)
    public void LoadMyScene()
    {
        SceneManager.LoadScene(sceneName);
    }

    // Update is called once per frame
    void Update()
    {
        // pressing [Esc] loads Scene
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LoadMyScene();
        }
    }
}
