﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorOff : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Sets Cursor to  be invisible
        Cursor.visible = false;
        // locks Cursor
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
