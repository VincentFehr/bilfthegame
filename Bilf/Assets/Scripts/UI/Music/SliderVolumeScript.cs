﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SliderVolumeScript : MonoBehaviour
{
    public AudioMixer myAudioMixer;

    public void SetMixerLevel(float sliderValue)
    {
        //Debug.Log("Set MixerLevel.");
        myAudioMixer.SetFloat("MusicVolume", Mathf.Log10(sliderValue) * 20);
        //Debug.Log("MicerLevel Set.");
    }
}
