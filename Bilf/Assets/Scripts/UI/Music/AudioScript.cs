﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioScript : MonoBehaviour
{
    // insert number of clips
    public AudioClip[] myClips;
    private AudioSource myAudioSource;
    
    // Start is called before the first frame update, makes myAudioSource being noticed as Audiosource in Game
    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        //Debug.Log("Audio source calibrated.");
    }

    // Update is called once per frame, if sound plays -> return, if no sound plays, start playing sound
    void Update()
    {
        if(myAudioSource.isPlaying)
        {
            return;
        }
        else
        {
            myAudioSource.clip = RandomClip();
            myAudioSource.Play();
            //Debug.Log("Started random clip.");
        }
    }
    // gets a random number out of the number of tracks in playlist
    AudioClip RandomClip ()
    {
        int randomNumber = Random.Range(0, myClips.Length);
        AudioClip randomClip = myClips[randomNumber];
        return randomClip;
    }
}
