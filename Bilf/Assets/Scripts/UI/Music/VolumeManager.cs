﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class VolumeManager : MonoBehaviour
{
    private static readonly string VolumePref = "VolumePref";
    public Slider volumeSlider;
    public Button saveButton;
    private float volumeFloat;

    
    
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("loading Button position.");
        volumeFloat = PlayerPrefs.GetFloat(VolumePref);
        volumeSlider.value = volumeFloat;
        //Debug.Log("Loading Volume complete.");
    }

    
}
