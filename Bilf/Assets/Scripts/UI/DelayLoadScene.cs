﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DelayLoadScene : MonoBehaviour
{
    // class to Load Scene with Delay (for Start Screen)

    // insert time for Delay here (3 = 3 Seconds)
    [SerializeField]
    public float delayTime;

    // insert Scene to Load here
    [SerializeField]
    public string sceneName;

    // Loads Scene (inserted sceneName)
    private void LoadMyScene()
    {
        SceneManager.LoadScene(sceneName);
    }

    // Update() is called once per frame
    void Update()
    {
        // delayedTime is reduced by one every second (because of Time.deltaTime), loads Scene when delayTime is <= 0
        delayTime -= Time.deltaTime;
        if (delayTime <= 0)
        {
            LoadMyScene();
        }
    }

}
