﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    // class to Quit the Game

    public void QuitTheGame()
    {
        Application.Quit();
        // Log Message to check if QuitTheGame() triggers in Editor
        Debug.Log("Game has been Quit.");
    }
}
