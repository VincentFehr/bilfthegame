﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenu : MonoBehaviour
{
    public static bool GameIsPaused;

    public GameObject MenuUI;
    public GameObject UITexts;
    
    // Start is called before the first frame update
    void Start()
    {
        GameIsPaused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (ButtonActions.SoundMenu)
            {
                Debug.Log("soundmenu was open, return:");
                return;
            }
            else
            {
                if (GameIsPaused)
                {
                Resume();
                }
                else
                {
                Pause();
                }
            }
            
        }
    }

    // Resumes Game: closes Ingame Menu, makes time go on, locks and blocks cursor
    public void Resume()
    {
        MenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        //Sets Cursor to  be invisible
        Cursor.visible = false;
        // locks Cursor
        Cursor.lockState = CursorLockMode.Locked;
        UITexts.SetActive(true);
    }

    // Pauses the Game and opens Ingame Menu: Sets cursor visible and unlocked, opens Menu UI Layer and pauses the Game
    void Pause()
    {
        UITexts.SetActive(false);
        //Sets Cursor to  be visible
        Cursor.visible = true;
        // unlocks Cursor
        Cursor.lockState = CursorLockMode.None;
        MenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
}
