﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNewScene : MonoBehaviour
{
    // class that Loads Scenes 

    // insert Scene to Load here
    [SerializeField]
    public string sceneName;

    // Loads Scene
    public void LoadMyScene()
    {
    SceneManager.LoadScene(sceneName);
    }
}
