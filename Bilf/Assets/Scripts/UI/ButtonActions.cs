﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ButtonActions : MonoBehaviour
{
    private static readonly string volumePref = "VolumePref";
    public Slider volumeSlider;
    public GameObject SoundUI;
    public GameObject MenuUI;
    public static bool SoundMenu = false;
  
    // saves Soundsettings
    public void SaveSoundSettings()
    {
        PlayerPrefs.SetFloat(volumePref, volumeSlider.value);
        PlayerPrefs.Save();
        Debug.Log("Soundsettings Saved.");
    }

    // insert Scene to Load here
    [SerializeField]
    public string sceneName;

    // Loads Scene
    public void LoadMyScene()
    {
        SceneManager.LoadScene(sceneName);
    }

    // Quits the Game
    public void QuitTheGame()
    {
        Application.Quit();
        // Log Message to check if QuitTheGame() triggers in Editor
        Debug.Log("Game has been Quit.");
    }

    // closes Menu, opens SoundUI
    public void OpenSoundSettings()
    {
        MenuUI.SetActive(false);
        SoundUI.SetActive(true);
        SoundMenu = true;
        Debug.Log("Soundmenu true.");
    }

    // saves Sound, closes SoundUI, opens Menu
    public void SaveAndBack()
    {
        SaveSoundSettings();
        SoundUI.SetActive(false);
        SoundMenu = false;
        MenuUI.SetActive(true);
    }
}
