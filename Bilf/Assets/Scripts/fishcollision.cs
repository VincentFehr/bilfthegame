﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishcollision : MonoBehaviour
{
    public int fishemission;
    private ParticleSystem ps;
    public int caughtfish;
    public ParticleSystem watersplash;

    private bool isplaying;


    public int intervalltimer;
    
    private void Start()
    {
        ps = this.GetComponent<ParticleSystem>();
        ps.maxParticles = fishemission;
       
        ps.Play();
        watersplash.Play();
        StartCoroutine("resetps");

    }

    private void Update()
    {

        if (!isplaying)
        {
            StartCoroutine("resetps");
        }
        else
        {
            return;
        }
        ps.maxParticles = fishemission;
    }
    private void OnParticleCollision(GameObject other)
    {
        fishemission = -1;
        caughtfish++;
    }
    public void resetparticlesystem()
    {
        ps.Play();
        watersplash.Play();
        
    }
    IEnumerator resetps()
    {
        isplaying = true;

        yield return new WaitForSeconds(intervalltimer);

        resetparticlesystem();

        isplaying = false;
    }
}
