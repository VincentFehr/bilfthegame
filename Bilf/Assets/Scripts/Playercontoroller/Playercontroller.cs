﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Playercontroller : MonoBehaviour
{   [SerializeField]
    private float _speed;
    [SerializeField]
    private float _sprintSpeed;
    [SerializeField]
    Rigidbody rb;
    [SerializeField]
    private float _rotateSpeed;
    
    private float _yRotation;
    [SerializeField]
    private GameObject _camera;
    [SerializeField]
    private Vector3 _awayDirection;
    [SerializeField]
    private float _normalSpeed;
    [SerializeField]
    private GameObject _cameraRotation;
    [SerializeField]
    private float _rotateCharacterSpeed = 5f;

    public GameObject slaybox;
    public float slaytime;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        _yRotation = 0f;
        _normalSpeed = _speed;

        slaybox.SetActive(false);

    }
    private void Update()
    {

        if (Input.GetKey(KeyCode.LeftShift))
        {
            _speed = _sprintSpeed;
            Debug.Log("sprintet");
        }
        else
        {
            _speed = _normalSpeed;

        }

        _awayDirection = this.transform.position - _camera.transform.position;
        _yRotation += Input.GetAxis("Mouse X")*90;
        playerMovement();
        
      
    }
    private void LateUpdate()
    {
        checkCamRotation();
        if (Input.GetMouseButtonDown(0))
        {
            
                slaybox.SetActive(true);
            StartCoroutine(Slaytimecollider());
            
        }
       
    }
    void playerMovement()
    {
        float hor = Input.GetAxisRaw("Horizontal");
        float ver = Input.GetAxisRaw("Vertical");

        Vector3 PlayerMovement = new Vector3(-hor, 0f, -ver).normalized * _speed * Time.deltaTime;
        transform.Translate(PlayerMovement, Space.Self);
        
    }
    void checkCamRotation()
    {
        if ( Input.GetButton("Vertical"))
        {
            //Debug.Log("rotates");
            //this.transform.localRotation = Quaternion.Euler(0, camera.transform.rotation.y, 0);
            //rb.transform.rotation.SetEulerAngles(0,camera.transform.rotation.eulerAngles.y,0);
            _cameraRotation.transform.eulerAngles = new Vector3(0, _camera.transform.eulerAngles.y + 180, 0);
            //this.transform.DOLookAt(awaydirection, rotateSpeed, AxisConstraint.Y);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, _cameraRotation.transform.rotation, Time.deltaTime* _rotateCharacterSpeed);
        }
        
    }
    IEnumerator Slaytimecollider()
    {
        Debug.Log("Schlag fängt an");
        yield return new WaitForSeconds(slaytime);
        slaybox.SetActive(false);
        Debug.Log("Schlag hört auf");
    }
   
}
