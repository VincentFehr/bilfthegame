using System.Collections;
using UnityEngine;

namespace Movement
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
     

        [Header("Walk / Run Setting")]
        [SerializeField]
        private float _walkSpeed;
        [SerializeField]
        private float _runSpeed;
        [SerializeField]
        private GameObject _graphicsObject;
        [SerializeField]
        private GameObject _moveObject;
        [SerializeField]
        private float _maxSpeed;

        [Header("Jump Settings")]
        [SerializeField]
        private float _playerJumpForce;
        [SerializeField]
        private ForceMode _appliedForceMode;

      
        [SerializeField]
        private float _rotateSpeed;
        [SerializeField]
        private GameObject _camera;
        [SerializeField]
        private GameObject _cameraRotation;

        [SerializeField]
        private GameObject slaybox;
        [SerializeField]
        private float _slaytime;
        [SerializeField]
        private float _rotateCharacterSpeed = 5f;


        [Header("Jumping State")]
        [SerializeField]
        private bool _playerIsJumping;

        [Header("Current Player Speed")]
        [SerializeField]
        private float _currentSpeed;

        [Header("Ground Check")]
        [Tooltip("A transform used to detect the ground")]
        [SerializeField]
        private Transform _groundCheckTransform = null;

        [Tooltip("The radius around transform which is used to detect the ground")]
        [SerializeField]
        private float _groundCheckRadius = 0.1f;

        [Tooltip("A layermask used to exclude/include certain layers from the \"ground\"")]
        [SerializeField]
        private LayerMask _groundCheckLayerMask = default;

        [Tooltip("A bool that shows if the player is on the ground")]
        [SerializeField]
        private bool _isGrounded;


        private float _xAxis;
        private float _zAxis;
        private Rigidbody _rb;
        private RaycastHit _hit;
        private Vector3 _groundLocation;
        private bool _leftShiftPressed;
        private int m_groundLayerMask;
        private float _distanceFromPlayerToGround;
        private bool _playerIsGrounded;
        private bool _playerJumpStarted;
     


        

        private const int MaxAllowJump = 1; //maximum allowed jumps
        private int _currentNumberOfJumpsMade; //current number of jumps processed

       
        
        private void Start()
        {          

            _rb = GetComponent<Rigidbody>();      
           
            
            _playerJumpStarted = true;

           
        }

        private void Update()
        {
          

            _xAxis = Input.GetAxis("Horizontal");
            _zAxis = Input.GetAxis("Vertical");

            float _speedMagnitude = Vector3.Magnitude(_rb.velocity); 

            if (_speedMagnitude > _maxSpeed)

            {
                float brakeSpeed = _speedMagnitude - _maxSpeed;  // calculate the speed decrease

                Vector3 normalisedVelocity = _rb.velocity.normalized;
                Vector3 brakeVelocity = normalisedVelocity * brakeSpeed;  // make the brake Vector3 value

                _rb.AddForce(-brakeVelocity);  // apply opposing brake force
            }

            _isGrounded = Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius, _groundCheckLayerMask);
            if (_isGrounded == true && _playerIsJumping == true)
            {
                StartCoroutine(ApplyJump());
            }
          

            _currentSpeed = _leftShiftPressed ? _runSpeed : _walkSpeed;

         

   

            _playerIsJumping = Input.GetKey(KeyCode.Space);

    

        

            _leftShiftPressed = Input.GetKey(KeyCode.LeftShift);
            

           
        }

        private void LateUpdate()
        {
            checkCamRotation();
            if (Input.GetMouseButtonDown(0))
            {

                slaybox.SetActive(true);
                StartCoroutine(Slaytimecollider());

            }

        }

        private void FixedUpdate()
        {            
                _rb.MovePosition(transform.position + Time.deltaTime * _currentSpeed *
                transform.TransformDirection(_xAxis, 0f, _zAxis));
                 

        }

        void checkCamRotation()
        {
           
                if (Input.GetKey(KeyCode.W))
                {
                    _cameraRotation.transform.eulerAngles = new Vector3(0, _camera.transform.eulerAngles.y, 0);

                    _graphicsObject.transform.rotation = Quaternion.Slerp(_graphicsObject.transform.rotation, _cameraRotation.transform.rotation, Time.deltaTime * _rotateCharacterSpeed);

                    var lookPos = transform.position - _camera.transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _rotateCharacterSpeed);

                }
                if (Input.GetKey(KeyCode.S))
                {
                    _cameraRotation.transform.eulerAngles = new Vector3(0, _camera.transform.eulerAngles.y + 180, 0);

                    _graphicsObject.transform.rotation = Quaternion.Slerp(_graphicsObject.transform.rotation, _cameraRotation.transform.rotation, Time.deltaTime * _rotateCharacterSpeed);

                    var lookPos = transform.position - _camera.transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _rotateCharacterSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {


                    _cameraRotation.transform.eulerAngles = new Vector3(0, _camera.transform.eulerAngles.y + 90, 0);

                    _graphicsObject.transform.rotation = Quaternion.Slerp(_graphicsObject.transform.rotation, _cameraRotation.transform.rotation, Time.deltaTime * _rotateCharacterSpeed);

                    var lookPos = transform.position - _camera.transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _rotateCharacterSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    _cameraRotation.transform.eulerAngles = new Vector3(0, _camera.transform.eulerAngles.y - 90, 0);

                    _graphicsObject.transform.rotation = Quaternion.Slerp(_graphicsObject.transform.rotation, _cameraRotation.transform.rotation, Time.deltaTime * _rotateCharacterSpeed);

                    var lookPos = transform.position - _camera.transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _rotateCharacterSpeed);
                }
            


        }


            /// <summary>
            /// applies force in the upward direction
            /// using jump force and supplied force mode
            /// </summary>
            /// <param name="jumpForce"></param>
            /// <param name="forceMode"></param>
            private void PlayerJump(float jumpForce, ForceMode forceMode)
            {
                _rb.AddForce(jumpForce * _rb.mass * Time.deltaTime * Vector3.up, forceMode);
            }

        /// <summary>
        /// handles single and double jump
        /// waits until space bar pressed is terminated before
        /// next jump is initiated.
        /// </summary>
        /// <returns></returns>
        private IEnumerator ApplyJump()
        {
            PlayerJump(_playerJumpForce, _appliedForceMode);
            _isGrounded = false;
            yield return new WaitUntil(() => !_playerIsJumping);
        }

        IEnumerator Slaytimecollider()
        {
            Debug.Log("Schlag f�ngt an");
            yield return new WaitForSeconds(_slaytime);
            slaybox.SetActive(false);
            Debug.Log("Schlag h�rt auf");
        }

    }
}