﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class Treefall : MonoBehaviour
{
    [SerializeField]
    private GameObject _uppertree;

    private Rigidbody _rb;
    Collider col;
    
   
    void Start()
    {
        _rb = _uppertree.GetComponent<Rigidbody>();

        _rb.isKinematic = true;
        //col = this.GetComponent<BoxCollider>();
    }


    //private void OnTriggerStay(Collider col)

    //{

    //}
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Baum wurde getroffen");

        if (Input.GetMouseButton(0))
            {
            if (other.gameObject.tag == "Frontslashcollider_Player")
             {
                Debug.Log("Baum wurde getroffen");
                _rb.isKinematic = false;
                _uppertree.tag = "PickUpTree";
                
            }
        }
    }

}
