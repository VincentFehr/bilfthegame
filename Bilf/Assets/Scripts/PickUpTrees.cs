﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTrees : MonoBehaviour
{
    [SerializeField]
    private GameObject _uppertree;

    private Rigidbody rb;
    [SerializeField]
    private GameObject _grabSpot;
    private bool _pickUp;
    private bool _dropIt;
    private bool _droppedTree;
    [SerializeField]
    private Collider col;
    [SerializeField]
    private CapsuleCollider treeCol;
    [SerializeField]
    private float _dropTimer = 0.2f;
    [SerializeField]
    private float _grabTimer = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        treeCol = this._uppertree.GetComponent<CapsuleCollider>();
        rb = this._uppertree.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        HoldTree();
        DropTree();
        PickMeUpAgain();
    }
    private void OnTriggerStay(Collider treeCol)
    {
        if (col.gameObject.tag == "Mouth")
        {
            if (this.tag == "PickUpTree" && Input.GetKey(KeyCode.F))
            {
                _pickUp = true;
            }
          
        }
    }

    private void PickMeUpAgain()
    {
        if(_droppedTree == true)
        {
            _grabTimer -= Time.deltaTime;

            if(_grabTimer <= 0f)
            {  
                _dropIt = false;
                _droppedTree = false;
            }
        }
    }
    private void DropTree()
    {
        if (_dropIt == true && Input.GetKey(KeyCode.F))
        {
            treeCol.enabled = true;
            rb.useGravity = true;
            _pickUp = false;
            _uppertree.transform.parent = null;
            _dropTimer = 0.2f;
            _droppedTree = true;           
        }
    }

    private void HoldTree()
    {
        if (_pickUp == true)
        {
            treeCol.enabled = false;
           _uppertree.transform.position = _grabSpot.transform.position;
           _uppertree.transform.rotation = _grabSpot.transform.rotation;
            _uppertree.transform.parent = _grabSpot.transform;
            rb.useGravity = false;
            _dropTimer -= Time.deltaTime;
            _grabTimer = 0.2f;

            if(_dropTimer <= 0f)
            { 
                _dropIt = true;
            }
        }
    }      
}
