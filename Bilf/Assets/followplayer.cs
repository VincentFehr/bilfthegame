﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class followplayer : MonoBehaviour
{
    public float followspeed;
    public GameObject Player;
    public Vector3 Offset;

    private void Update()
    {
        this.transform.position = Player.transform.position + Offset;
    }
}
